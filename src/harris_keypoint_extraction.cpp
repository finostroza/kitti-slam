/* \author Bastian Steder, Modified by Felipe Inostroza */

#include <iostream>

#include <boost/filesystem.hpp>
#include <boost/thread/thread.hpp>
#include <pcl/range_image/range_image.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/range_image_visualizer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/features/range_image_border_extractor.h>
#include <pcl/keypoints/harris_3d.h>
#include <pcl/keypoints/sift_keypoint.h>
#include <pcl/keypoints/iss_3d.h>
#include <pcl/console/parse.h>
#include <pcl/common/transforms.h>

#include <pcl/features/normal_3d_omp.h>
#include <pcl/features/shot_omp.h>

typedef pcl::PointXYZI PointType;

// --------------------
// -----Parameters-----
// --------------------


bool use_visualization = false;

double radius=0.5;
int min_points=40;
double threshold = 0.02;
bool calcSHOT=false;


double
computeCloudResolution (const pcl::PointCloud<PointType>::ConstPtr &cloud)
{
	double res = 0.0;
	int n_points = 0;
	int nres;
	std::vector<int> indices (2);
	std::vector<float> sqr_distances (2);
	pcl::search::KdTree<PointType> tree;
	tree.setInputCloud (cloud);

	for (size_t i = 0; i < cloud->size (); ++i)
	{
		if (! pcl_isfinite ((*cloud)[i].x))
		{
			continue;
		}
		//Considering the second neighbor since the first is the point itself.
		nres = tree.nearestKSearch (i, 2, indices, sqr_distances);
		if (nres == 2)
		{
			res += sqrt (sqr_distances[1]);
			++n_points;
		}
	}
	if (n_points != 0)
	{
		res /= n_points;
	}
	return res;
}

// --------------
// -----Help-----
// --------------
void
printUsage (const char* progName)
{
	std::cout << "\n\nUsage: "<<progName<<" [options] <poses.txt> <scene0.pcd> <scene1.pcd> ...\n\n"
			<< "Options:\n"
			<< "-------------------------------------------\n"
			<< "-v           Visualize keypoints\n"
			<< "-s           Calculate SHOT352 descriptors\n"
			<< "-r <double>  Radius for harris detection (default "<< (double)radius<<")\n"
			<< "-p <int>     Minimum number of points for harris detection (default "<< (int)min_points<<")\n"
			<< "-t <double>  Threshold for harris detection (default "<< (double)threshold<<")\n"
			<< "-h           this help\n"
			<< "\n\n";
}



void visualize_keypoints (const pcl::PointCloud<pcl::PointXYZI>::Ptr points,
		const pcl::PointCloud<pcl::PointWithScale>::Ptr keypoints)
{
	// Add the points to the vizualizer
	pcl::visualization::PCLVisualizer viz;
	pcl::visualization::PointCloudColorHandlerGenericField<PointType> point_cloud_color_handler(points, "intensity");
	//pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZI> point_cloud_color_handler (points, 0, 255, 0);
	viz.addPointCloud (points,point_cloud_color_handler, "points");

	// Draw each keypoint as a sphere
	for (size_t i = 0; i < keypoints->size (); ++i)
	{
		// Get the point data
		const pcl::PointWithScale & p = keypoints->points[i];

		// Pick the radius of the sphere *
		float r = 2 * p.scale;
		// * Note: the scale is given as the standard deviation of a Gaussian blur, so a
		//   radius of 2*p.scale is a good illustration of the extent of the keypoint

		// Generate a unique string for each sphere
		std::stringstream ss ("keypoint");
		ss << i;

		// Add a sphere at the keypoint
		viz.addSphere (p, 2*p.scale, 1.0, 0.0, 0.0, ss.str ());
	}

	// Give control over to the visualizer
	viz.spin ();
}


// --------------
// -----Main-----
// --------------
int
main (int argc, char** argv)
{
	// --------------------------------------
	// -----Parse Command Line Arguments-----
	// --------------------------------------
	if (pcl::console::find_argument (argc, argv, "-h") >= 0)
	{
		printUsage (argv[0]);
		return 0;
	}
	if (pcl::console::find_argument (argc, argv, "-v") >= 0)
	{
		use_visualization = true;
		cout << "----Visualization activated----   beep-boop I am a robot.\n";
	}
	if (pcl::console::find_argument (argc, argv, "-s") >= 0)
		{
			calcSHOT = true;
			cout << "----SHOT descriptors enabled----.\n";
		}
	if (pcl::console::parse (argc, argv, "-r", radius) >= 0)
		cout << "Setting radius to "<<radius<<"m.\n";
	if (pcl::console::parse (argc, argv, "-t", threshold) >= 0)
		cout << "Setting threshold to "<<threshold<<".\n";
	if (pcl::console::parse (argc, argv, "-p", min_points) >= 0)
		cout << "Setting minimum points to "<<min_points<<".\n";




	// ------------------------------------------------------------------
	// -----Read poses file -----
	// ------------------------------------------------------------------
	std::vector<int> poses_filename_indices = pcl::console::parse_file_extension_argument (argc, argv, "txt");

	std::vector< Eigen::Matrix4f> transforms;
	Eigen::Matrix4f velodyne2camera;
	velodyne2camera << 7.027555e-03, -9.999753e-01, 2.599616e-05, -4.069766e-03, -2.254837e-03, -4.184312e-05, -9.999975e-01, -7.631618e-02, 9.999728e-01, 7.027479e-03, -2.255075e-03, -2.717806e-01, 0, 0, 0, 1;
	std::cout << "reading poses file" << std::endl;
	if(!poses_filename_indices.empty()){
		std::string filename = argv[poses_filename_indices[0]];
		std::ifstream poses_file;
		poses_file.open(filename.c_str());
		while(!poses_file.eof()){
			Eigen::Matrix4f transform=Eigen::Matrix4f::Identity(), transform_inverse;
			for(int i=0;i<3;i++)
				for(int j=0; j<4;j++)
					poses_file >>transform(i,j);

			transform_inverse=transform.inverse();
			transforms.push_back(transform*velodyne2camera);
			//std::cout << "transform: \n" << transform*velodyne2camera<< std::endl;
		}


	}
	std::cout << "poses file read" << std::endl;
	// ------------------------------------------------------------------
	// -----Read pcd file or create example point cloud if not given-----
	// ------------------------------------------------------------------
	std::cout << "reading pcd files" << std::endl;

	pcl::visualization::PCLVisualizer::Ptr viewer ;
    if (use_visualization){
    	viewer.reset(  new pcl::visualization::PCLVisualizer("Keypoints"));

    }

	pcl::PCDWriter writer;

	std::vector<int> pcd_filename_indices = pcl::console::parse_file_extension_argument (argc, argv, "pcd");
	for (int i=0;i<pcd_filename_indices.size();i++)
	{
		std::string filename = argv[pcd_filename_indices[i]];
		boost::filesystem::path path(filename);
		int pointcloud_number = atoi(path.stem().c_str());
		pcl::PointCloud<PointType>::Ptr point_cloud_ptr (new pcl::PointCloud<PointType>);
		pcl::PointCloud<PointType>::Ptr point_cloud_transformed_ptr (new pcl::PointCloud<PointType>);
		pcl::PointCloud<PointType>& point_cloud = *point_cloud_ptr;
		pcl::PointCloud<PointType>& point_cloud_transformed = *point_cloud_transformed_ptr;
		if (pcl::io::loadPCDFile (filename, point_cloud_transformed) == -1)
		{
			cerr << "Was not able to open file \""<<filename<<"\".\n";
			printUsage (argv[0]);
			return 0;
		}
		if(transforms.size()>pointcloud_number){
			pcl::transformPointCloud (*point_cloud_transformed_ptr, *point_cloud_ptr, transforms[pointcloud_number]);

		}
		else point_cloud =point_cloud_transformed;


		double model_resolution=0.1;
		pcl::search::KdTree<pcl::PointXYZI>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZI> ());
		// Object for storing the normals.
		pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>);
		// Object for storing the SHOT descriptors for each point.
		pcl::PointCloud<pcl::SHOT352>::Ptr descriptors(new pcl::PointCloud<pcl::SHOT352>());
		// Estimate the normals.
		pcl::NormalEstimationOMP<pcl::PointXYZI, pcl::Normal> normalEstimation;
		normalEstimation.setNumberOfThreads(8);
		normalEstimation.setInputCloud(point_cloud_ptr);
		normalEstimation.setRadiusSearch(6*model_resolution);

		normalEstimation.setSearchMethod(tree);
		normalEstimation.compute(*normals);
		// --------------------------------
		// -----Extract HARRIS 3D keypoints-----
		// --------------------------------



		pcl::HarrisKeypoint3D<pcl::PointXYZI,pcl::PointXYZI> detector;
		detector.setNonMaxSupression (true);
		detector.setRadius (radius);
		detector.setThreshold(threshold);
		detector.setRefine(true);
		//detector.setRadiusSearch (100);
		detector.setSearchMethod(tree);
		detector.setNumberOfThreads(8);
		detector.setNormals(normals);
		detector.setInputCloud(point_cloud_ptr);
		pcl::PointCloud<pcl::PointXYZI>::Ptr keypoints(new pcl::PointCloud<pcl::PointXYZI>());

		detector.compute(*keypoints);


		// filter minimum number of points
		pcl::PointCloud<pcl::PointXYZI>::Ptr keypoints_filtered(new pcl::PointCloud<pcl::PointXYZI>());

		for(int i =0; i< keypoints->size(); i++){
		      std::vector<int> nn_indices;
		      std::vector<float> nn_dists;
			tree->radiusSearch((*keypoints)[i],radius,nn_indices,nn_dists);
			if(nn_indices.size() >= min_points){
				keypoints_filtered->push_back(keypoints->at(i));
			}
		}
		keypoints=keypoints_filtered;


		std::cout << i+1 << "/"<< pcd_filename_indices.size()<<"  keypoints detected: " << keypoints->size() << "                  \r" << std::flush;





		writer.write<pcl::PointXYZI> (path.stem().string()+"_keypoints.pcd",*keypoints,true);

		// --------------------------------------------
		// -----Calculate SHOT descriptors-----
		// --------------------------------------------

		// SHOT estimation object.
		if (calcSHOT){
		pcl::SHOTEstimationOMP<pcl::PointXYZI, pcl::Normal, pcl::SHOT352> shot;
		shot.setNumberOfThreads(8);
		shot.setInputCloud(keypoints);
		shot.setInputNormals(normals);
		shot.setSearchMethod(tree);
		// The radius that defines which of the keypoint's neighbors are described.
		// If too large, there may be clutter, and if too small, not enough points may be found.
		shot.setRadiusSearch(radius);
		shot.setSearchSurface(point_cloud_ptr);
		shot.compute(*descriptors);

		writer.write<pcl::SHOT352> (path.stem().string()+"_descriptor.pcd",*descriptors,true);
		}
		// --------------------------------------------
		// -----Open 3D viewer and add point cloud-----
		// --------------------------------------------


		if (use_visualization){
		viewer->setBackgroundColor (0, 0, 0);
		viewer->addCoordinateSystem (1.0f, "global");
		//pcl::visualization::PointCloudColorHandlerGenericField<PointType> point_cloud_color_handler(point_cloud_ptr, "intensity");
		//pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZI> point_cloud_color_handler (point_clouds[i], 0, 255, 0);
		pcl::visualization::PointCloudColorHandlerRandom<pcl::PointXYZI> point_cloud_color_handler (point_cloud_ptr);
		std::stringstream ptcloudname;
		ptcloudname << "originalPointCloud " << i;
		//viewer.addPointCloud (point_clouds[i], point_cloud_color_handler, ptcloudname.str());







		// -------------------------------------
		// -----Show keypoints in 3D viewer-----
		// -------------------------------------

		pcl::visualization::PointCloudColorHandlerRandom<pcl::PointXYZI> keypoints_color_handler (keypoints);
		std::stringstream keypointsname;
		keypointsname << "keypoints " << i;
		viewer->addPointCloud<pcl::PointXYZI> (keypoints, keypoints_color_handler, keypointsname.str());
		//viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 6, keypointsname.str());
		}

	}
	// -----------------------------------------------
	// ----- Reset Camera

	//viewer.initCameraParameters ();


	//--------------------
	// -----Main loop-----
	//--------------------
	if (use_visualization){
	while (!viewer->wasStopped ())
	{

		viewer->spinOnce ();
		pcl_sleep(0.01);
	}

	}

}
