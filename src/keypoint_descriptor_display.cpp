
/* \author  Felipe Inostroza */

#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/io/pcd_io.h>
#include <pcl/keypoints/iss_3d.h>
#include <pcl/console/parse.h>
#include <pcl/features/shot_omp.h>
#include <pcl/common/transforms.h>
#include <string.h>
#include <pcl/ml/kmeans.h>
#include <opencv2/opencv.hpp>
#include <opencv2/ml/ml.hpp>
#include <random>

#include <pcl/segmentation/extract_clusters.h>

typedef pcl::PointXYZI PointType;
typedef pcl::SHOT352 DescriptorType;


void
printUsage (const char* progName)
{
	std::cout << "\n\nUsage: "<<progName<<" [options] <XXX_keypoints.pcd>\n\n"
			<< "Options:\n"
			<< "-------------------------------------------\n"
			<< "-h           this help\n"
			<< "\n\n";
}


void copySHOTtoOpenCV(pcl::PointCloud<DescriptorType>::Ptr descriptor_cloud_ptr, cv::Mat* openCVDesc){

	if(openCVDesc->rows!= descriptor_cloud_ptr->size()){
		openCVDesc->create(descriptor_cloud_ptr->size(),352,CV_32F);
	}

    for(int i = 0; i< descriptor_cloud_ptr->size(); i++){

    	for(int j =0; j<352;j++)
    	  openCVDesc->at<float>(i,j) = descriptor_cloud_ptr->at(i).descriptor[j];
    }

}

void copyKeypointstoOpenCV(pcl::PointCloud<PointType>::Ptr cloud_ptr, cv::Mat* openCVKeypoints){

	if(openCVKeypoints->rows!= cloud_ptr->size()){
		openCVKeypoints->create(cloud_ptr->size(),3,CV_32F);
	}

    for(int i = 0; i< cloud_ptr->size(); i++){

    	for(int j =0; j<3;j++)
    	  openCVKeypoints->at<float>(i,j) = cloud_ptr->at(i).data[j];
    }

}

void copyKeypointsFromOpenCV(cv::Mat* openCVKeypoints, pcl::PointCloud<PointType>::Ptr cloud_ptr){

	if(openCVKeypoints->rows!= cloud_ptr->size()){
		cloud_ptr->resize(openCVKeypoints->rows);
	}
    for(int i = 0; i< cloud_ptr->size(); i++){

    	for(int j =0; j<3;j++)
    		cloud_ptr->at(i).data[j] = openCVKeypoints->at<double>(i,j);
    }


}

// --------------
// -----Main-----
// --------------
int
main (int argc, char** argv)
{
	// ------------------------------------------------------------------
	// -----Read poses file -----
	// ------------------------------------------------------------------
	std::vector<int> poses_filename_indices = pcl::console::parse_file_extension_argument (argc, argv, "txt");

	std::vector< Eigen::Matrix4f> transforms;
	Eigen::Matrix4f velodyne2camera;
	velodyne2camera << 7.027555e-03, -9.999753e-01, 2.599616e-05, -4.069766e-03, -2.254837e-03, -4.184312e-05, -9.999975e-01, -7.631618e-02, 9.999728e-01, 7.027479e-03, -2.255075e-03, -2.717806e-01, 0, 0, 0, 1;
	std::cout << "reading poses file" << std::endl;
	if(!poses_filename_indices.empty()){
		std::string filename = argv[poses_filename_indices[0]];
		std::ifstream poses_file;
		poses_file.open(filename.c_str());
		while(!poses_file.eof()){
			Eigen::Matrix4f transform=Eigen::Matrix4f::Identity(), transform_inverse;
			for(int i=0;i<3;i++)
				for(int j=0; j<4;j++)
					poses_file >>transform(i,j);

			transform_inverse=transform.inverse();
			transforms.push_back(transform*velodyne2camera);
			//std::cout << "transform: \n" << transform*velodyne2camera<< std::endl;
		}


	}
	std::cout << "poses file read" << std::endl;

	// ------------------------------------------------------------------
	// -----Read pcd files-----
	// ------------------------------------------------------------------
	std::cout << "reading pcd files" << std::endl;
	std::vector<pcl::PointCloud<PointType>::Ptr> keypoints;
	std::vector<pcl::PointCloud<DescriptorType>::Ptr> descriptors;





	std::vector<int> pcd_filename_indices = pcl::console::parse_file_extension_argument (argc, argv, "_keypoints.pcd");

	for (int i=0;i<pcd_filename_indices.size();i++)
	{
		std::string filename = argv[pcd_filename_indices[i]];
		boost::filesystem::path path(filename);
		int pointcloud_number = atoi(path.stem().c_str());
		pcl::PointCloud<PointType>::Ptr point_cloud_ptr (new pcl::PointCloud<PointType>);
		pcl::PointCloud<PointType>::Ptr point_cloud_transformed_ptr (new pcl::PointCloud<PointType>);
		pcl::PointCloud<PointType>& point_cloud = *point_cloud_ptr;
		if (pcl::io::loadPCDFile (filename, point_cloud) == -1)
		{
			cerr << "Was not able to open file \""<<filename<<"\".\n";
			printUsage (argv[0]);
			return 0;
		}
		if(transforms.size()>pointcloud_number){
			pcl::transformPointCloud (*point_cloud_ptr, *point_cloud_transformed_ptr, transforms[pointcloud_number]);
			keypoints.push_back(point_cloud_transformed_ptr);
		}
		else keypoints.push_back(point_cloud_ptr);
		std::string descriptor_filename=filename.substr(0,filename.size()-14);

		descriptor_filename.append("_descriptor.pcd");
		pcl::PointCloud<DescriptorType>::Ptr descriptor_cloud_ptr (new pcl::PointCloud<DescriptorType>);
		pcl::PointCloud<DescriptorType>& descriptor_cloud = *descriptor_cloud_ptr;
		if (pcl::io::loadPCDFile (descriptor_filename, descriptor_cloud) == -1)
				{
					cerr << "Was not able to open file \""<<descriptor_filename<<"\".\n";
					printUsage (argv[0]);
					return 0;
				}
		descriptors.push_back(descriptor_cloud_ptr);

	}

	std::cout << "pcd files read" << std::endl;

    //Accumulate into big clouds
	pcl::PointCloud<DescriptorType>::Ptr all_descriptors_ptr (new pcl::PointCloud<DescriptorType>);
	pcl::PointCloud<DescriptorType>& all_descriptors = *all_descriptors_ptr;
	pcl::PointCloud<PointType>::Ptr all_keypoints_ptr (new pcl::PointCloud<PointType>);
	pcl::PointCloud<PointType>& all_keypoints = *all_keypoints_ptr;

	for(int i=0; i< keypoints.size(); i++){
		all_descriptors += *descriptors[i];
		all_keypoints += *keypoints[i];
	}


	// Use Expectation Maximization to get means of keypoints!

	int numKeypoints = 3;

	cv::Mat all_descriptors_opencv(all_descriptors_ptr->size(),352, CV_32F);
	copySHOTtoOpenCV(all_descriptors_ptr, &all_descriptors_opencv);


    cv::Ptr<cv::ml::EM> model=cv::ml::EM::create() ;

    model->setClustersNumber(numKeypoints);
    model->setCovarianceMatrixType(cv::ml::EM::Types::COV_MAT_DIAGONAL);
    //ramdomly select points as mean values for GM components
    /*cv::Mat initialMeans(numKeypoints,3, CV_64F);

    std::default_random_engine generator;
    std::uniform_int_distribution<int> distribution(0, numKeypoints-1);


    for (int i =0; i< numKeypoints; i++){
    	int  npoint = distribution(generator);
    	for(int j =0; j<3;j++){
    		initialMeans.at<float>(i,j) = all_keypoints.at(i).data[j];
    	}
    }*/
    cv::Mat logLikelihoods,labels;

    model->trainEM(all_descriptors_opencv,logLikelihoods,labels);

    cv::Mat means_opencv=model->getMeans();

	pcl::PointCloud<PointType>::Ptr means_ptr (new pcl::PointCloud<PointType>);
	pcl::PointCloud<PointType>& means = *means_ptr;

    copyKeypointsFromOpenCV(&means_opencv, means_ptr);





    std::vector<pcl::PointCloud<PointType>::Ptr> keypoints_by_label;
    for(int i=0; i<numKeypoints; i++){
    	pcl::PointCloud<PointType>::Ptr point_cloud_ptr (new pcl::PointCloud<PointType>);
    	keypoints_by_label.push_back(point_cloud_ptr);
    }
    for(int i=0; i<all_keypoints.size();i++){
    	int l = labels.at<int>(i,0); //label
    	keypoints_by_label[l]->push_back(all_keypoints[i]);
    }







	pcl::visualization::PCLVisualizer viewer ("3D Viewer");
	pcl::visualization::PointCloudColorHandlerRandom<PointType> keypoints_color_handler (all_keypoints_ptr);


	viewer.setBackgroundColor (0, 0, 0);
	viewer.addCoordinateSystem (1.0f, "global");

	for(int i =0; i<numKeypoints; i++){
		std::stringstream keypointsname;
		keypointsname << "keypoints " << i;
		viewer.addPointCloud<PointType> (keypoints_by_label[i], keypoints_color_handler, keypointsname.str());
		viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 4, keypointsname.str());
	}
	pcl::visualization::PointCloudColorHandlerRandom<PointType> means_color_handler (means_ptr);
	//viewer.addPointCloud<PointType> (means_ptr, means_color_handler, "Means");
	//viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 6, "Means");
	//viewer.addPointCloud<PointType> (all_keypoints_ptr, keypoints_color_handler, "all");
	std::cout << all_keypoints_ptr->size() << std::endl;
	std::cout << means_ptr->size() << std::endl;
	for(int i=0; i< means.size(); i++)
	 std::cout << means[i] << std::endl;
	while (!viewer.wasStopped ())
	{

		viewer.spinOnce ();
		pcl_sleep(0.01);
	}

}

