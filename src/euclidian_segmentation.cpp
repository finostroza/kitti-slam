
/* \author  Felipe Inostroza */

#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/io/pcd_io.h>
#include <pcl/keypoints/iss_3d.h>
#include <pcl/console/parse.h>
#include <pcl/features/shot_omp.h>
#include <pcl/common/transforms.h>
#include <pcl/common/distances.h>
#include <string.h>
#include <pcl/ml/kmeans.h>
#include <opencv2/opencv.hpp>
#include <opencv2/ml/ml.hpp>
#include <random>

#include <pcl/segmentation/extract_clusters.h>

typedef pcl::PointXYZI PointType;
typedef pcl::SHOT352 DescriptorType;

// --------------------
// -----Parameters-----
// --------------------

bool use_visualization = false;
bool save_file = false;
double max_cluster_radius=1.0;
std::string save_filename;


void
printUsage (const char* progName)
{
	std::cout << "\n\nUsage: "<<progName<<" [options] <XXX_keypoints.pcd>\n\n"
			<< "Options:\n"
			<< "-------------------------------------------\n"
			<< "-v            Visualize keypoints\n"
			<< "-s <filename> Save result to output DO NOT USE .pcd extension\n"
			<< "-h           this help\n"
			<< "\n\n";
}

unsigned int text_id = 0;
void keyboardEventOccurred (const pcl::visualization::KeyboardEvent &event,
                            void* viewer_void)
{
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = *static_cast<boost::shared_ptr<pcl::visualization::PCLVisualizer> *> (viewer_void);
  if (event.getKeySym () == "r" && event.keyDown ())
  {
    std::cout << "r was pressed => removing all text" << std::endl;

    char str[512];
    for (unsigned int i = 0; i < text_id; ++i)
    {
      sprintf (str, "text#%03d", i);
      viewer->removeShape (str);
    }
    text_id = 0;
  }
}


// --------------
// -----Main-----
// --------------
int
main (int argc, char** argv)
{
	// --------------------------------------
	// -----Parse Command Line Arguments-----
	// --------------------------------------
	if (pcl::console::find_argument (argc, argv, "-h") >= 0)
	{
		printUsage (argv[0]);
		return 0;
	}
	if (pcl::console::find_argument (argc, argv, "-v") >= 0)
	{
		use_visualization = true;
		cout << "----Visualization activated----   beep-boop I am a robot.\n";
	}
	if (pcl::console::parse (argc, argv, "-s", save_filename) >= 0){
		save_file= true;
		cout << "Saving result to "<<save_filename<<".\n";
	}
	// ------------------------------------------------------------------
	// -----Read poses file -----
	// ------------------------------------------------------------------
	std::vector<int> poses_filename_indices = pcl::console::parse_file_extension_argument (argc, argv, "txt");

	std::vector< Eigen::Matrix4f> transforms;
	Eigen::Matrix4f velodyne2camera;
	velodyne2camera << 7.027555e-03, -9.999753e-01, 2.599616e-05, -4.069766e-03, -2.254837e-03, -4.184312e-05, -9.999975e-01, -7.631618e-02, 9.999728e-01, 7.027479e-03, -2.255075e-03, -2.717806e-01, 0, 0, 0, 1;
	std::cout << "reading poses file" << std::endl;
	if(!poses_filename_indices.empty()){
		std::string filename = argv[poses_filename_indices[0]];
		std::ifstream poses_file;
		poses_file.open(filename.c_str());
		while(!poses_file.eof()){
			Eigen::Matrix4f transform=Eigen::Matrix4f::Identity(), transform_inverse;
			for(int i=0;i<3;i++)
				for(int j=0; j<4;j++)
					poses_file >>transform(i,j);

			transform_inverse=transform.inverse();
			transforms.push_back(transform*velodyne2camera);
			//std::cout << "transform: \n" << transform*velodyne2camera<< std::endl;
		}


	}
	std::cout << "poses file read" << std::endl;

	// ------------------------------------------------------------------
	// -----Read pcd files-----
	// ------------------------------------------------------------------
	std::cout << "reading pcd files" << std::endl;
	std::vector<pcl::PointCloud<PointType>::Ptr> keypoints;
	std::vector<pcl::PointCloud<DescriptorType>::Ptr> descriptors;





	std::vector<int> pcd_filename_indices = pcl::console::parse_file_extension_argument (argc, argv, ".pcd");

	for (int i=0;i<pcd_filename_indices.size();i++)
	{
		std::string filename = argv[pcd_filename_indices[i]];
		boost::filesystem::path path(filename);
		int pointcloud_number = atoi(path.stem().c_str());
		pcl::PointCloud<PointType>::Ptr point_cloud_ptr (new pcl::PointCloud<PointType>);
		pcl::PointCloud<PointType>::Ptr point_cloud_transformed_ptr (new pcl::PointCloud<PointType>);
		pcl::PointCloud<PointType>& point_cloud = *point_cloud_ptr;
		if (pcl::io::loadPCDFile (filename, point_cloud) == -1)
		{
			cerr << "Was not able to open file \""<<filename<<"\".\n";
			printUsage (argv[0]);
			return 0;
		}
		if(transforms.size()>pointcloud_number){
			pcl::transformPointCloud (*point_cloud_ptr, *point_cloud_transformed_ptr, transforms[pointcloud_number]);
			keypoints.push_back(point_cloud_transformed_ptr);
		}
		else keypoints.push_back(point_cloud_ptr);

	}

	std::cout << "pcd files read" << std::endl;

    //Accumulate into big clouds

	pcl::PointCloud<PointType>::Ptr all_keypoints_ptr (new pcl::PointCloud<PointType>);
	pcl::PointCloud<PointType>& all_keypoints = *all_keypoints_ptr;

	for(int i=0; i< keypoints.size(); i++){

		all_keypoints += *keypoints[i];
	}



	//----------segment keypoints into clusters-------------

	// Creating the KdTree object for the search method of the extraction
	pcl::search::KdTree<PointType>::Ptr tree (new pcl::search::KdTree<PointType>);
	tree->setInputCloud (all_keypoints_ptr);
	std::vector<pcl::PointIndices> cluster_indices;
	pcl::EuclideanClusterExtraction<PointType> ec;
	ec.setClusterTolerance (0.2); // 2cm
	ec.setMinClusterSize (5);
	ec.setMaxClusterSize (2500);
	ec.setSearchMethod (tree);
	ec.setInputCloud (all_keypoints_ptr);
	ec.extract (cluster_indices);

	pcl::visualization::PCLVisualizer::Ptr viewer ;
	if (use_visualization){
		viewer.reset(  new pcl::visualization::PCLVisualizer("Keypoints"));
		viewer->setBackgroundColor (0, 0, 0);
		viewer->addCoordinateSystem (1.0f, "global");

	}
	int j = 0;
	for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it)
	{
		pcl::PointCloud<PointType>::Ptr cloud_cluster (new pcl::PointCloud<PointType>);
		for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit)
			cloud_cluster->points.push_back (all_keypoints_ptr->points[*pit]); //*
		cloud_cluster->width = cloud_cluster->points.size ();
		cloud_cluster->height = 1;
		cloud_cluster->is_dense = true;

		std::cout << "PointCloud representing the Cluster: " << cloud_cluster->points.size () << " data points." << std::endl;
		std::stringstream ss;
		ss << "cloud_cluster_" << j << ".pcd";
		if (use_visualization){
			pcl::visualization::PointCloudColorHandlerRandom<pcl::PointXYZI> cluster_color_handler (cloud_cluster);
			viewer->addPointCloud<pcl::PointXYZI> (cloud_cluster, cluster_color_handler, ss.str());
			viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 6, ss.str());
		}


		j++;
	}

	// -------------------------------------
	// -----Show keypoints in 3D viewer-----
	// -------------------------------------


	if (use_visualization){




		for (int i =0; i<keypoints.size(); i++){
			pcl::visualization::PointCloudColorHandlerRandom<pcl::PointXYZI> keypoints_color_handler (keypoints[i]);
			std::stringstream keypointsname;
			keypointsname << "keypoints " << i ;
			viewer->addPointCloud<pcl::PointXYZI> (keypoints[i], keypoints_color_handler, keypointsname.str());
			//viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 6, keypointsname.str());
		}
	}


	//--------------------
	// -----Main loop-----
	//--------------------
	if (use_visualization){
		while (!viewer->wasStopped ())
		{

			viewer->spinOnce ();
			pcl_sleep(0.01);
		}

	}

	if(save_file){

		pcl::PointCloud<PointType>::Ptr cluster_means (new pcl::PointCloud<PointType>);

		for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it){
			PointType p ;
			p.x=0;
			p.y=0;
			p.z=0;
			p.intensity=0;
			int n_p=0;
			for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit){
				p.x+=all_keypoints_ptr->points[*pit].x;
				p.y+=all_keypoints_ptr->points[*pit].y;
				p.z+=all_keypoints_ptr->points[*pit].z;
				p.intensity+=all_keypoints_ptr->points[*pit].intensity;
				n_p++;
			}
			p.x=p.x/n_p;
			p.y=p.y/n_p;
			p.z=p.z/n_p;
			p.intensity=p.intensity/n_p;
			bool distance_too_large=false;
			for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit){
				if(pcl::euclideanDistance(p,all_keypoints_ptr->points[*pit])> max_cluster_radius)
					distance_too_large=true;
			}

			if (!distance_too_large)
				cluster_means->push_back(p);

		}
		pcl::PCDWriter writer;
		writer.write<PointType> (save_filename,*cluster_means,true);

	}


}

