/* \author Bastian Steder, Modified by Felipe Inostroza */

#include <iostream>

#include <boost/filesystem.hpp>
#include <boost/thread/thread.hpp>
#include <pcl/range_image/range_image.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/range_image_visualizer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/features/range_image_border_extractor.h>
#include <pcl/keypoints/harris_3d.h>
#include <pcl/keypoints/sift_keypoint.h>
#include <pcl/console/parse.h>
#include <pcl/common/transforms.h>

typedef pcl::PointXYZI PointType;

// --------------------
// -----Parameters-----
// --------------------
float angular_resolution = 0.5f;
float support_size = 0.2f;
pcl::RangeImage::CoordinateFrame coordinate_frame = pcl::RangeImage::CAMERA_FRAME;
bool setUnseenToMaxRange = false;
bool rotation_invariant = true;

// --------------
// -----Help-----
// --------------
void 
printUsage (const char* progName)
{
  std::cout << "\n\nUsage: "<<progName<<" [options] <scene.pcd>\n\n"
            << "Options:\n"
            << "-------------------------------------------\n"
            << "-r <float>   angular resolution in degrees (default "<<angular_resolution<<")\n"
            << "-c <int>     coordinate frame (default "<< (int)coordinate_frame<<")\n"
            << "-m           Treat all unseen points to max range\n"
            << "-s <float>   support size for the interest points (diameter of the used sphere - "
                                                                  "default "<<support_size<<")\n"
            << "-o <0/1>     switch rotational invariant version of the feature on/off"
            <<               " (default "<< (int)rotation_invariant<<")\n"
            << "-h           this help\n"
            << "\n\n";
}

void 
setViewerPose (pcl::visualization::PCLVisualizer& viewer, const Eigen::Affine3f& viewer_pose)
{
  Eigen::Vector3f pos_vector = viewer_pose * Eigen::Vector3f (0, 0, 0);
  Eigen::Vector3f look_at_vector = viewer_pose.rotation () * Eigen::Vector3f (0, 0, 1) + pos_vector;
  Eigen::Vector3f up_vector = viewer_pose.rotation () * Eigen::Vector3f (0, -1, 0);
  viewer.setCameraPosition (pos_vector[0], pos_vector[1], pos_vector[2],
                            look_at_vector[0], look_at_vector[1], look_at_vector[2],
                            up_vector[0], up_vector[1], up_vector[2]);
}

void visualize_keypoints (const pcl::PointCloud<pcl::PointXYZI>::Ptr points,
                          const pcl::PointCloud<pcl::PointWithScale>::Ptr keypoints)
{
  // Add the points to the vizualizer
  pcl::visualization::PCLVisualizer viz;
  pcl::visualization::PointCloudColorHandlerGenericField<PointType> point_cloud_color_handler(points, "intensity");
  //pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZI> point_cloud_color_handler (points, 0, 255, 0);
  viz.addPointCloud (points,point_cloud_color_handler, "points");

  // Draw each keypoint as a sphere
  for (size_t i = 0; i < keypoints->size (); ++i)
  {
    // Get the point data
    const pcl::PointWithScale & p = keypoints->points[i];

    // Pick the radius of the sphere *
    float r = 2 * p.scale;
    // * Note: the scale is given as the standard deviation of a Gaussian blur, so a
    //   radius of 2*p.scale is a good illustration of the extent of the keypoint

    // Generate a unique string for each sphere
    std::stringstream ss ("keypoint");
    ss << i;

    // Add a sphere at the keypoint
    viz.addSphere (p, 2*p.scale, 1.0, 0.0, 0.0, ss.str ());
  }

  // Give control over to the visualizer
  viz.spin ();
}


// --------------
// -----Main-----
// --------------
int 
main (int argc, char** argv)
{
  // --------------------------------------
  // -----Parse Command Line Arguments-----
  // --------------------------------------
  if (pcl::console::find_argument (argc, argv, "-h") >= 0)
  {
    printUsage (argv[0]);
    return 0;
  }
  if (pcl::console::find_argument (argc, argv, "-m") >= 0)
  {
    setUnseenToMaxRange = true;
    cout << "Setting unseen values in range image to maximum range readings.\n";
  }
  if (pcl::console::parse (argc, argv, "-o", rotation_invariant) >= 0)
    cout << "Switching rotation invariant feature version "<< (rotation_invariant ? "on" : "off")<<".\n";
  int tmp_coordinate_frame;
  if (pcl::console::parse (argc, argv, "-c", tmp_coordinate_frame) >= 0)
  {
    coordinate_frame = pcl::RangeImage::CoordinateFrame (tmp_coordinate_frame);
    cout << "Using coordinate frame "<< (int)coordinate_frame<<".\n";
  }
  if (pcl::console::parse (argc, argv, "-s", support_size) >= 0)
    cout << "Setting support size to "<<support_size<<".\n";
  if (pcl::console::parse (argc, argv, "-r", angular_resolution) >= 0)
    cout << "Setting angular resolution to "<<angular_resolution<<"deg.\n";
  angular_resolution = pcl::deg2rad (angular_resolution);
  


  // ------------------------------------------------------------------
  // -----Read poses file -----
  // ------------------------------------------------------------------
  std::vector<int> poses_filename_indices = pcl::console::parse_file_extension_argument (argc, argv, "txt");

  std::vector< Eigen::Matrix4f> transforms;
  Eigen::Matrix4f velodyne2camera;
  velodyne2camera << 7.027555e-03, -9.999753e-01, 2.599616e-05, -4.069766e-03, -2.254837e-03, -4.184312e-05, -9.999975e-01, -7.631618e-02, 9.999728e-01, 7.027479e-03, -2.255075e-03, -2.717806e-01, 0, 0, 0, 1;
  std::cout << "reading poses file" << std::endl;
  if(!poses_filename_indices.empty()){
	  std::string filename = argv[poses_filename_indices[0]];
	  std::ifstream poses_file;
	  poses_file.open(filename.c_str());
	  while(!poses_file.eof()){
		  Eigen::Matrix4f transform=Eigen::Matrix4f::Identity(), transform_inverse;
		  for(int i=0;i<3;i++)
			  for(int j=0; j<4;j++)
				  poses_file >>transform(i,j);

		  transform_inverse=transform.inverse();
		  transforms.push_back(transform*velodyne2camera);
		  //std::cout << "transform: \n" << transform*velodyne2camera<< std::endl;
	  }


  }
  std::cout << "poses file read" << std::endl;
  // ------------------------------------------------------------------
  // -----Read pcd file or create example point cloud if not given-----
  // ------------------------------------------------------------------
  std::vector<pcl::PointCloud<PointType>::Ptr> point_clouds;



  std::vector<int> pcd_filename_indices = pcl::console::parse_file_extension_argument (argc, argv, "pcd");
  for (int i=0;i<pcd_filename_indices.size();i++)
  {
    std::string filename = argv[pcd_filename_indices[i]];
    boost::filesystem::path path(filename);
    int pointcloud_number = atoi(path.stem().c_str());
    pcl::PointCloud<PointType>::Ptr point_cloud_ptr (new pcl::PointCloud<PointType>);
    pcl::PointCloud<PointType>::Ptr point_cloud_transformed_ptr (new pcl::PointCloud<PointType>);
    pcl::PointCloud<PointType>& point_cloud = *point_cloud_ptr;
    if (pcl::io::loadPCDFile (filename, point_cloud) == -1)
    {
      cerr << "Was not able to open file \""<<filename<<"\".\n";
      printUsage (argv[0]);
      return 0;
    }
    if(transforms.size()>pointcloud_number){
    	 pcl::transformPointCloud (*point_cloud_ptr, *point_cloud_transformed_ptr, transforms[pointcloud_number]);
    	 point_clouds.push_back(point_cloud_transformed_ptr);
    }
    else point_clouds.push_back(point_cloud_ptr);

  }
  




  pcl::visualization::PCLVisualizer viewer ("3D Viewer");

  
for(int i=0; i<point_clouds.size(); i++ ){
  // --------------------------------
  // -----Extract Harris3D keypoints-----
  // --------------------------------

  pcl::HarrisKeypoint3D<pcl::PointXYZI,pcl::PointXYZI> detector;
  detector.setNonMaxSupression (true);
  detector.setRadius (1.0);
  detector.setThreshold(0.01);
  detector.setRefine(true);
  //detector.setRadiusSearch (100);
  detector.setInputCloud(point_clouds[i]);
  pcl::PointCloud<pcl::PointXYZI>::Ptr keypoints(new pcl::PointCloud<pcl::PointXYZI>());

  detector.compute(*keypoints);
  std::cout << "keypoints detected: " << keypoints->size() << std::endl;

  /*

  pcl::PointCloud<pcl::PointWithScale>::Ptr keypoints(new pcl::PointCloud<pcl::PointWithScale>());
  pcl::SIFTKeypoint<pcl::PointXYZI, pcl::PointWithScale> sift_detect;
  // Use a FLANN-based KdTree to perform neighborhood searches
  sift_detect.setSearchMethod(pcl::search::KdTree<pcl::PointXYZI>::Ptr(new pcl::search::KdTree<pcl::PointXYZI>));
  // Set the detection parameters
  sift_detect.setScales (0.1, 4, 5);
  sift_detect.setMinimumContrast (0.03);
  // Set the input
  sift_detect.setInputCloud (point_cloud_ptr);
  sift_detect.compute(*keypoints);
  std::cout << "keypoints detected: " << keypoints->size() << std::endl;

  
 visualize_keypoints(point_cloud_ptr, keypoints);
*/

  // ----------------------------------------------
  // -----Show keypoints in range image widget-----
  // ----------------------------------------------
  //for (size_t i=0; i<keypoint_indices.points.size (); ++i)
    //range_image_widget.markPoint (keypoint_indices.points[i]%range_image.width,
                                  //keypoint_indices.points[i]/range_image.width);
  
  // --------------------------------------------
  // -----Open 3D viewer and add point cloud-----
  // --------------------------------------------

  std::cout << "starting viewer" << std::endl;

  viewer.setBackgroundColor (0, 0, 0);
  viewer.addCoordinateSystem (1.0f, "global");
  //pcl::visualization::PointCloudColorHandlerGenericField<PointType> point_cloud_color_handler(point_cloud_ptr, "intensity");
  //pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZI> point_cloud_color_handler (point_clouds[i], 0, 255, 0);
  pcl::visualization::PointCloudColorHandlerRandom<pcl::PointXYZI> point_cloud_color_handler (point_clouds[i]);
  std::stringstream ptcloudname;
  ptcloudname << "originalPointCloud " << i;
  //viewer.addPointCloud (point_clouds[i], point_cloud_color_handler, ptcloudname.str());


  std::cout << "viewerstarted" << std::endl;




  // -------------------------------------
  // -----Show keypoints in 3D viewer-----
  // -------------------------------------
  //pcl::visualization::PointCloudColorHand~/Code/kitti-slam/build/narf_feature_extraction 000000.pcdlerCustom<pcl::PointWithScale> keypoints_color_handler (keypoints, 255, 0, 0);
  //viewer.addPointCloud<pcl::PointWithScale> (keypoints, keypoints_color_handler, "keypoints");
  //viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 4, "keypoints");

  //pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZI> keypoints_color_handler (keypoints, 255, 0, 0);

  pcl::visualization::PointCloudColorHandlerRandom<pcl::PointXYZI> keypoints_color_handler (keypoints);
  std::stringstream keypointsname;
  keypointsname << "keypoints " << i;
  viewer.addPointCloud<pcl::PointXYZI> (keypoints, keypoints_color_handler, keypointsname.str());
  viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 6, keypointsname.str());

}
  // -----------------------------------------------
  // ----- Reset Camera

  //viewer.initCameraParameters ();
  

  //--------------------
  // -----Main loop-----
  //--------------------
  while (!viewer.wasStopped ())
  {

    viewer.spinOnce ();
    pcl_sleep(0.01);
  }
}
